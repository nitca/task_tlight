SEX_MALE = 'MALE'
SEX_FEMALE = 'FEMALE'
SEX_UNKNOWN = 'UNKNOWN'
SEX = [
    (SEX_MALE, 'мужской'),
    (SEX_FEMALE, 'женский'),
    (SEX_UNKNOWN, 'неизвестно'),
]

TYPE_FRS = "FRS"
TYPE_RPT = "FRS"
TYPE_OSD = "FRS"
TYPE_IRC = "FRS"
TYPE = [
    (TYPE_FRS, 'первичный'),
    (TYPE_RPT, 'повторный'),
    (TYPE_OSD, 'внешний'),
    (TYPE_IRC, 'косвенный'),
]

ROLE_CLIENT = 'CLIENT'
ROLE_ENTITY = 'ENTITY'
ROLE_DEPART = 'DEPART'

ROLE = [
    (ROLE_CLIENT, 'клиент'),
    (ROLE_ENTITY, 'юр_лицо'),
    (ROLE_DEPART, 'департмент'),
]
