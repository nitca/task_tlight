from django.test import TestCase
from custom_user import models


class UserTest(TestCase):
    def test_wrong_role(self):
        user = models.User(username='123', password='123', role='123')
        self.assertEqual(user.username, '123')
        with self.assertRaises(ValueError):
            user.save()

    def test_client(self):
        client_phone_num = '+7123456789'
        client_full_name = 'Ивано Иван Иванович'
        client_type = 'FRS'
        client_timezone = 'UTC+3'

        user = models.User.objects.create(
            username='test_client', password='test', role='client'
        )
        user.client.phone_num = client_phone_num
        user.client.full_name = client_full_name
        user.client_type = client_type
        user.client.timezone = client_timezone

        self.assertEqual(user.client.id, 101)
        self.assertEqual(user.client.phone_num, client_phone_num)
        self.assertEqual(user.client.full_name, client_full_name)
        self.assertEqual(user.client.client_type, client_type)
        self.assertEqual(user.client.timezone, client_timezone)

    def test_entity(self):
        entity_full_name = 'OrgOrgProgramm'
        entity_short_name = 'OOP'
        entity_inn = '123456789'
        entity_kpp = '1234567890'

        user = models.User.objects.create(
            username='test_entity', password='123', role='entity'
        )
        user.entity.full_name = entity_full_name
        user.entity.short_name = entity_short_name
        user.entity.inn = entity_inn
        user.entity.kpp = entity_kpp

        self.assertEqual(user.entity.id, 102)
        self.assertEqual(user.entity.full_name, entity_full_name)
        self.assertEqual(user.entity.short_name, entity_short_name)
        self.assertEqual(user.entity.inn, entity_inn)
        self.assertEqual(user.entity.kpp, entity_kpp)


class ClientTest(TestCase):
    @classmethod
    def setUpClass(self):
        self.phone_num = "+71234567890"
        self.phone_nums = ['+71234567897', '+74613789089']
        self.full_name = "Василий Васильев Васечкин"
        self.client_type = 'первичный'
        self.sex = 'мужской'
        self.timezone = 'UTC+3'
        self.social_vks = [
            'https://vk.com/hello_world', 'https://vk.com/hello_you_too'
        ]
        self.social_fbs = [
            'https://fb.com/hello_world', 'https://fb.com/hello_you_too'
        ]
        self.ok = 'https://ok.ru/id12345'
        self.instagram = 'https://instagram.com/hello'
        self.telegram = 'https://t.me/hello_world'
        self.whatsapp = '+71234567890'
        self.viber = '+71234567890'

        client = models.Client.objects.create(
            phone_number=self.phone_num,
            full_name=self.full_name,
            client_type=self.client_type,
            timezone=self.timezone,
            sex=self.sex
        )

        client.add_socials(
            ok=self.ok,
            instagram=self.instagram,
            telegram=self.telegram,
            whatsapp=self.whatsapp,
            viber=self.viber
        )

        client.socials.add_vk(*self.social_vks)

        client.socials.add_fb(*self.social_fbs)

        client.add_sec_phone_nums(*self.phone_nums)

    def test_client_data(self):
        client = models.Client.objects.first()

        self.assertEqual(client.id, 101)
        self.assertEqual(client.phone_number, self.phone_num)
        self.assertEqual(client.full_name, self.full_name)
        self.assertEqual(client.client_type, self.client_type)
        self.assertEqual(client.sex, self.sex)

        for number in client.secondary_phone_numbers.all():
            self.assertIn(number.phone_number, self.phone_nums)

    def test_social_data(self):
        client = models.Client.objects.first()

        self.assertEqual(client.timezone, self.timezone)
        self.assertEqual(client.socials.whatsapp, self.whatsapp)
        self.assertEqual(client.socials.ok, self.ok)
        self.assertEqual(client.socials.telegram, self.telegram)
        self.assertEqual(client.socials.viber, self.viber)
        self.assertEqual(client.socials.instagram, self.instagram)

        for vk in client.socials.vk.all():
            self.assertIn(vk.vk, self.social_vks)

        for fb in client.socials.facebook.all():
            self.assertIn(fb.facebook, self.social_fbs)

    @classmethod
    def tearDownClass(self):
        models.Client.objects.all().delete()


class EntityTest(TestCase):

    @classmethod
    def setUpClass(self):
        self.client_phone_num = "+71234567890"
        self.client_phone_nums = ['+71234567897', '+74613789089']
        self.client_full_name = "Василий Васильев Васечкин"
        self.client_client_type = 'первичный'
        self.client_sex = 'мужской'
        self.client_timezone = 'UTC+3'
        self.client_social_vks = [
            'https://vk.com/hello_world', 'https://vk.com/hello_you_too'
        ]
        self.client_social_fbs = [
            'https://fb.com/hello_world', 'https://fb.com/hello_you_too'
        ]
        self.client_ok = 'https://ok.ru/id12345'
        self.client_instagram = 'https://instagram.com/hello'
        self.client_telegram = 'https://t.me/hello_world'
        self.client_whatsapp = '+71234567890'
        self.client_viber = '+71234567890'

        self.full_name = "Фулл компани"
        self.short_name = "Шрт ком"
        self.inn = "1234567890"
        self.kpp = "1234567890"

    def test_entity(self):
        entity = models.Entity.objects.create(
            full_name=self.full_name,
            short_name=self.short_name,
            inn=self.inn,
            kpp=self.kpp
        )

        self.assertEqual(entity.id, 102)
        self.assertEqual(entity.full_name, self.full_name)
        self.assertEqual(entity.short_name, self.short_name)
        self.assertEqual(entity.inn, self.inn)
        self.assertEqual(entity.kpp, self.kpp)

    def test_client(self):
        entity = models.Entity.objects.create(
            full_name=self.full_name,
            short_name=self.short_name,
            inn=self.inn,
            kpp=self.kpp
        )
        client = entity.client.create(
            phone_number=self.client_phone_num,
            client_type=self.client_client_type,
            sex=self.client_sex,
            timezone=self.client_timezone
            )

        client.add_socials(
            ok=self.client_ok,
            instagram=self.client_instagram,
            telegram=self.client_telegram,
            whatsapp=self.client_whatsapp,
            viber=self.client_viber
        )

        client.socials.add_vk(*self.client_social_vks)

        client.socials.add_fb(*self.client_social_fbs)

        client.add_sec_phone_nums(*self.client_phone_nums)

        client = models.Entity.objects.first().client.get()

        self.assertEqual(self.client_phone_num, client.phone_number)
        self.assertEqual(self.client_client_type, client.client_type)
        self.assertEqual(self.client_sex, client.sex)
        self.assertEqual(self.client_timezone, client.timezone)

        for num in client.secondary_phone_numbers.all():
            self.assertIn(num.phone_number, self.client_phone_nums)

    @classmethod
    def tearDownClass(self):
        models.Client.objects.all().delete()
