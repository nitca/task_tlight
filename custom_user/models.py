from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver

from custom_user import choices
from django.db import models


def get_new_client_id():
    """Generates custom id for client with 01 in end"""
    last_model = Client.objects.all().order_by('id').last()
    if not last_model:
        return 101
    else:
        return int(str(last_model.id // 100 + 1) + '01')


def get_new_entity_id():
    """Custom id for entity with 02 in end"""
    last_model = Entity.objects.all().order_by('id').last()
    if not last_model:
        return 102
    else:
        return int(str(last_model.id // 100 + 1) + '02')


class User(AbstractUser):
    role = models.CharField(
        verbose_name='роль', max_length=16, choices=choices.ROLE
    )


# <-------------CLIENT------------------->
class Client(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, null=True, related_name='client'
    )
    id = models.BigIntegerField(
        primary_key=True, default=get_new_client_id, unique=True
    )
    phone_number = models.CharField(
        verbose_name='основной телефон', max_length=16, unique=True
    )
    full_name = models.CharField(verbose_name='ФИО', max_length=256)
    date_creation = models.DateField(
        verbose_name='дата создания', auto_now_add=True
    )
    date_change = models.DateField(
        verbose_name='дата изменения', auto_now=True
    )
    date_status_change = models.DateField(
        verbose_name='дата изменения статуса', auto_now=True
    )
    status = models.BooleanField(verbose_name='статус', default=False)
    client_type = models.CharField(
        verbose_name='тип',
        max_length=32,
        choices=choices.TYPE,
        default=choices.TYPE_FRS
        )
    sex = models.CharField(
        verbose_name='пол',
        max_length=16,
        choices=choices.SEX,
        default=choices.SEX_FEMALE
    )
    timezone = models.CharField(verbose_name='часовой пояс', max_length=8)

    socials = models.ForeignKey(
        'ClientSocials',
        on_delete=models.CASCADE,
        verbose_name='социальные сети',
        null=True
    )

    secondary_phone_numbers = models.ManyToManyField(
        'ClientPhoneNumber', verbose_name='дополнительный телефонный номер'
    )
    email = models.ManyToManyField('ClientEmail', verbose_name='email')

    def add_sec_phone_nums(self, *phone_nums):
        for phone_num in phone_nums:
            self.secondary_phone_numbers.create(phone_number=phone_num)

    def add_socials(self, **socials):
        self.socials = ClientSocials.objects.create(**socials)
        self.save()


class ClientSocials(models.Model):
    ok = models.CharField(verbose_name='ok', max_length=64)
    instagram = models.CharField(
        verbose_name='instagram', max_length=64, null=True
    )
    telegram = models.CharField(
        verbose_name='telegram', max_length=64, null=True
    )
    whatsapp = models.CharField(
        verbose_name='whatsapp', max_length=16, null=True
        )
    viber = models.CharField(
        verbose_name='viber', max_length=16, null=True
        )

    vk = models.ManyToManyField('ClientSocialVk', verbose_name='vk')

    facebook = models.ManyToManyField(
        'ClientSocialFacebook', verbose_name='facebook'
    )

    def add_vk(self, *vks):
        for vk in vks:
            self.vk.create(vk=vk)

    def add_fb(self, *fbs):
        for fb in fbs:
            self.facebook.create(facebook=fb)


class ClientSocialVk(models.Model):
    vk = models.CharField(max_length=64)


class ClientSocialFacebook(models.Model):
    facebook = models.CharField(verbose_name='facebook', max_length=64)


class ClientPhoneNumber(models.Model):
    phone_number = models.CharField(max_length=16)


class ClientEmail(models.Model):
    email = models.EmailField(verbose_name='email')


# <--------------ENTITY------------------------>
class Entity(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, null=True, related_name='entity'
    )
    id = models.BigIntegerField(
        primary_key=True, default=get_new_entity_id, unique=True
    )
    date_creation = models.DateField(
        verbose_name='дата создания', auto_now_add=True
    )
    date_change = models.DateField(
        verbose_name='дата изменения', auto_now=True
    )
    full_name = models.CharField(
        verbose_name='полное название', max_length=128
    )
    short_name = models.CharField(
        verbose_name='сокращённое название', max_length=64
    )
    inn = models.CharField(verbose_name='иин', max_length=64)
    kpp = models.CharField(verbose_name='кпп', max_length=64)
    client = models.ManyToManyField(Client)

    def get_clients(self):
        return self.client.all()


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        if instance.role == 'client':
            Client.objects.create(user=instance)
        elif instance.role == 'entity':
            Entity.objects.create(user=instance)
        else:
            raise ValueError(
                'Must be set user\'s role: client, entity or department'
            )


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    if instance.role == 'client':
        instance.client.save()
    elif instance.role == 'entity':
        instance.entity.save()
    else:
        raise ValueError(
            'Must be set user\'s role: client, entity or department'
        )
